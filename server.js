const express = require("express")
const app = express()
const cors = require("cors");
const path = require("path")
const fs = require("fs")

//token
const conversorJson = require("body-parser")
const jwt = require("jsonwebtoken")
const SECRET = "SohEuSei"

//convertendo os dados do corpo da requisição para JavaScript Object  
app.use(conversorJson.urlencoded({ extended: false }))
app.use(conversorJson.json())

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

app.use(express.static(path.join(__dirname, "public")));

/**
 * criando a porta do servidor
 */
const porta = 3001
app.listen(porta, function () {
    console.log("Servidor rodando na porta " + porta);
})

/**
 * mapeando caminho do servidor 
 */
app.get("/", function (req, resp) {
    resp.sendFile(__dirname + "/views/index.html")
})


/**
 * Salvando os dados no pesquisa.csv
 */

app.post("/pesquisa", function (req, resp) {

    // console.log(req.body.a);
    resultado = `${req.body.a},${req.body.b},${req.body.c},${req.body.d},${req.body.e},${req.body.f},${req.body.date},\n`
    fs.appendFile("pesquisa.csv", `${resultado} ,\n`, function (err) {
        if (err) {
            resp.json({
                Status: "500",
                mensagem: "Erro ao registrar a pesquisa , contate o administrador do sistema" + err,
            });
        } else {
            resp.json({
                Status: "200",
                mensagem: "Pesquisa Registrada Com sucesso",
            });
        }
    });
});


/**
 * estatíticas da pesquisa de satisfação
 */

app.get("/estatisticas", function (req, resp) {


    fs.readFile('pesquisa.csv', 'utf8', function (err, data) {


        if (err) {
            throw err //console.log("Erro ao ler arquivo: " + err);
        } else {

            var dados = data.split("\n")
            var pesquisas = []

            dados.forEach(element => {
                pesquisas.push(element.split(","))

            });

            var teste = calcularEstatistica(pesquisas)

            console.log(teste);
        }
        console.log(pesquisas);
        resp.send(teste)

    })
});


/**
 * calculo isolado para a apuracao das respostas
 */


function calcularEstatistica(pesquisas) {

    let questaoA = ["Não foi atendida", "Parcialmente atendida", "totalmente atendida"]
    let qtA = [0, 0, 0]

    ///------------------
    let questaoB = ["Péssimo", "Ruim", "Aceitavel", "Bom", "Excelente"]
    let qtB = [0, 0, 0, 0, 0]

    ///------------------

    let questaoC = ["Indelicado", "Mau Humorado", "Neutro", "Educado", "Muito Atencioso"]
    let qtC = [0, 0, 0, 0, 0]


    /**
     * nota de 0 a 5
     * nota de 6 a 10
     */
    ///------------------

    let questaoD = [0, 0]

    //idade e 3 requisitos
    let questaoE = [0, 0, 0, 0, 0]


    ///------------------


    let questaoF = ["Feminino", "Masculino", "LGBT"]
    let qtF = [0, 0, 0]

    ///------------------

    var total = pesquisas.length

    for (let i = 0; i < pesquisas.length; i++) {

        ///QUESTAO A
        if (pesquisas[i][0] == questaoA[0]) {
            qtA[0]++
        } else if (pesquisas[i][0] == questaoA[1]) {
            qtA[1]++
        } else if (pesquisas[i][0] == questaoA[2]) {
            qtA[2]++
        }

        ///QUESTAO B
        if (pesquisas[i][1] == questaoB[0]) {
            qtB[0]++
        } else if (pesquisas[i][1] == questaoB[1]) {
            qtB[1]++
        } else if (pesquisas[i][1] == questaoB[2]) {
            qtB[2]++
        } else if (pesquisas[i][1] == questaoB[3]) {
            qtB[3]++
        } else if (pesquisas[i][1] == questaoB[4]) {
            qtB[4]++
        }

        ///QUESTAO C
        if (pesquisas[i][2] == questaoC[0]) {
            qtC[0]++
        } else if (pesquisas[i][2] == questaoC[1]) {
            qtC[1]++
        } else if (pesquisas[i][2] == questaoC[2]) {
            qtC[2]++
        } else if (pesquisas[i][2] == questaoC[3]) {
            qtC[3]++
        } else if (pesquisas[i][2] == questaoC[4]) {
            qtC[4]++
        }

        //QUESTAO D

        if (pesquisas[i][3] += 0 && pesquisas[i][3] <= 5) {
            questaoD[0]++
        } else if (pesquisas[i][3] += 6 && pesquisas[i][3] <= 10) {
            questaoD[1]++
        }

        //QUESTAO E

        if (pesquisas[i][4] < 15) {
            questaoE[0]++
        } else if (pesquisas[i][4] >= 15 && pesquisas[i][4] <= 21) {
            questaoE[1]++
        } else if (pesquisas[i][4] >= 22 && pesquisas[i][4] <= 35) {
            questaoE[2]++
        } else if (pesquisas[i][4] >= 36 && pesquisas[i][4] <= 50) {
            questaoE[3]++
        } else {
            questaoE[4]++
        }


        ///QUESTAO F
        if (pesquisas[i][5] == questaoF[0]) {
            qtF[0]++
        } else if (pesquisas[i][5] == questaoF[1]) {
            qtF[1]++
        } else if (pesquisas[i][5] == questaoF[2]) {
            qtF[2]++
        }

    }

    let estatiticas = {
        a: {
            naoFoiAtendida: ((100 * qtA[0]) / total),
            parcialmenteAtendida: ((100 * qtA[1]) / total),
            totalmenteAtendida: ((100 * qtA[2]) / total),
        },

        b: {
            pessimo: ((100 * qtB[0]) / total),
            ruim: ((100 * qtB[1]) / total),
            aceitavel: ((100 * qtB[2]) / total),
            bom: ((100 * qtB[3]) / total),
            excelente: ((100 * qtB[4]) / total),

        },

        c: {
            indelicado: ((100 * qtC[0]) / total),
            neutro: ((100 * qtC[1]) / total),
            educado: ((100 * qtC[2]) / total),
            muitoAtencioso: ((100 * qtC[3]) / total),
        },
        d: {
            notaEntre0a5: ((questaoD[0] * 100) / total),
            notaEntre6a10: ((questaoD[1] * 100) / total)
        },

        e: {
            idadeMenosDe15Anos: ((questaoE[0] * 100) / total),
            idadeEntre15e21: ((questaoE[1] * 100) / total),
            idadeEntre22e35: ((questaoE[2] * 100) / total),
            idadeEntre36e50: ((questaoE[3] * 100) / total),
            idadeAcimaDe50: ((questaoE[4] * 100) / total),
        },

        f: {
            feminino: ((100 * qtF[0]) / total),
            masculino: ((100 * qtF[1]) / total),
            lGBT: ((100 * qtF[2]) / total),
        }



    }

    console.log(estatiticas);
    return estatiticas

}



/**
 * relatório criado através da resposta da requisição
 * Tela de consulta de estatísticas
 *Com uma Template String
 */

app.get("/relatorio", function (req, resp) {

    fs.readFile('pesquisa.csv', 'utf8', function (err, data) {
        if (err) {
            throw err
        } else {
            var dados = data.split("\n")

            var pesquisas = []
            dados.forEach(element => {
                pesquisas.push(element.split(","))
            });

            var teste = calcularEstatistica(pesquisas)

            resp.send(
                `
                    <!DOCTYPE html>
                        <html lang="en">
                        <head>
                            <meta charset="UTF-8">
                            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <title>Estatísticas</title>
                        </head>
                        <body>
                            <h2>Estatísticas - Total de Pesquisas enviadas: ${pesquisas.length}</h2>
                            <div>
                                <h4>Sua solicitação foi atendida?</h4>
                                <ul>
                                    <li>Não foi atendida: ${teste.a.naoFoiAtendida} % </li>
                                    <li>Parcialmente Atendida: ${teste.a.parcialmenteAtendida} %</li>
                                    <li>Totalmente Atendida: ${teste.a.totalmenteAtendida} %</li>
                                </ul>
                            </div>
                            <div>
                                <h4>Qual nota você daria para o atendimento?</h4>
                                <ul>
                                    <li>Péssimo:  ${teste.b.pessimo} % </li>
                                    <li>Ruim: ${teste.b.ruim} % </li>
                                    <li>Aceitável: ${teste.b.aceitavel} % </li>
                                    <li>Bom: ${teste.b.bom} % </li>
                                    <li>Excelente:${teste.b.excelente} % </li>
                                </ul>
                            </div>
                            <div>
                                <h4>Como você classificaria o comportamento do atendente?</h4>
                                <ul>
                                    <li>Indelicado:  ${teste.c.indelicado} % </li>
                                    <li>Mau Humorado:  ${teste.c.neutro} % </li>
                                    <li>Neutro:  ${teste.c.educado} % </li>
                                    <li>Educado:   ${teste.c.indelicado} % </li>
                                    <li>Muito Atencioso:  ${teste.c.muitoAtencioso} % </li>
                                </ul>
                            </div>
                            <div>

                            <div>
                                <h4>De 0 à 10, qual nota você daria para o produto?</h4>
                                <ul>
                                    <li>Nota de 0 a 5  ${teste.d.notaEntre0a5}</li>
                                    <li>Nota de 6 a 10 ${teste.d.notaEntre6a10}</li>
                                </ul>
                            </div>
                                <h4>Idade</h4>
                                <ul>
                                    <li> menos de 15 anos: ${teste.e.idadeMenosDe15Anos} % </li>
                                    <li>entre 15 e 21 anos:${teste.e.idadeEntre15e21} % </li>
                                    <li>entre 22 e 35 anos:${teste.e.idadeEntre22e35} % </li>
                                    <li>entre 36 e 50 anos:${teste.e.idadeEntre36e50} % </li>
                                    <li>acima de 50 anos: ${teste.e.idadeAcimaDe50} % </li>
                                </ul>
                            </div>
                            <div>
                                <h4>Gênero</h4>
                                <ul>
                                    <li>Feminino: ${teste.f.feminino} % </li>
                                    <li>Masculino: ${teste.f.masculino} % </li>
                                    <li>LGBT: ${teste.f.lGBT} % </li>
                                </ul>
                            </div>
                        </body>
                    </html>
                            `

            )

        }
    })
})




/**
 *  nova rota ([GET]/login).
 * retorna a pagina index.html
 */
app.get("/login", function (req, resp) {


    resp.sendFile(__dirname + "/views/login.html")

})


/**
 * nova rota POST para a requisição de login
 * 
 */
app.post("/login", function (req, resp) {


    console.log(req.body.nomeUser);
    fs.readFile('usuarios.csv', 'utf8', function (err, data) {


        if (err) {
            throw err
        } else {

            var dados = data.split("\n")
            var usuarios = []

            dados.forEach(element => {
                usuarios.push(element.split(","))

            });
            var isUsuarioAutenticado = autenticacao(req.body.nomeUser, req.body.passUser, usuarios)
            console.log(isUsuarioAutenticado);

            if (isUsuarioAutenticado) {
                //gerar token 
                const token = jwt.sign({ xxx: req.body.nomeUser }, SECRET, { expiresIn: 600 })
                return resp.json({ auth: true, token })
            }

            resp.status(401).end()
        }

    })

})


/**
 * 
 */

function autenticacao(nomeUser, passUser, usuarios) {
    for (let i = 0; i < usuarios.length; i++) {
        if (nomeUser == usuarios[i][0] && passUser == usuarios[i][1]) {
            return true
        }

    }
}



function verifyJWT(req, resp, next) {
    const token = req.header("x-access-token")
    jwt.verify(token, SECRET, function (err, decoded) {
        if (err) {
            return resp.status(401).end()
        }
        req.dec = decoded.xxx
        next()
    })
}
