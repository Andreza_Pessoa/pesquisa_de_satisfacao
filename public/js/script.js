       
        
        async function pesquisa(options) {
          var response = await fetch(`http://localhost:3001/pesquisa`, options)
          return response.json();
      }
      async function btnEnviar() {

          const update = {
              
              a: document.querySelector('input[name=nmSolAt]:checked').value,
              b: document.querySelector('input[name=nmSoli]:checked').value,
              c: document.querySelector('input[name=nmComp]:checked').value,
              d: document.getElementById("idRange1").value,
              e: document.getElementById("idIdade").value,    
              f: document.querySelector('input[name=nmsexo]:checked').value,
              date: new Date().getTime()
          };
          var aut = localStorage.getItem("token")
          console.log(aut);

          const options = {
              method: "POST",
              headers: {
                  "Content-Type": "application/json",
                  "x-acess-token": aut
              },
              body: JSON.stringify(update),
          };
            
          /*
          *mensagem para o usuario com settimeout
          */
          let response = await pesquisa(options);
          console.log(response);

          var mensagem = document.getElementById("idMensagem");

          if (response.Status == 200) {
              mensagem.innerHTML = response.mensagem;
              mensagem.classList.add("msnTrue");

              setTimeout(function () {
                  mensagem.parentNode.removeChild(mensagem);
                  location.reload();
              }, 2000);
          } else {
              mensagem.innerHTML = response.mensagem;
              mensagem.classList.add("msnErro");
          }
      }


/**
 * retornar a estatíticas da pesquisa de satisfação
 * recebendo dados da pesquisa.csv
 */
// async function calculoEstatisticas(){
//     pesquisaContabilizada = await estatisticas();
//     console.log(pesquisaContabilizada);
// }

 async function estatisticas() {
    var response = await fetch(`http://localhost:3001/estatisticas`)
    return response.json();
}

/**
 * 
 */

 async function calculoEstatisticas(){
    pesquisaContabilizada = await relatorio();
    console.log(pesquisaContabilizada);
}

 async function relatorio() {
    var response = await fetch(`http://localhost:3001/relatorio`)
    return response.json();
}